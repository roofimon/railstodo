class Task < ApplicationRecord
    after_initialize :set_defaults, unless: :persisted?
    def set_defaults
        self.completed ||= false
    end
    validates :title, presence: true
end

class FixColumnName < ActiveRecord::Migration[7.1]
  def self.up
    rename_column :tasks, :status, :completed
  end
end
